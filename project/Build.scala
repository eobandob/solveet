import sbt._
import Keys._


/**
  Author: Londo
  Company: Ciris Informatic Solutions	
*/
object SolveetBuild extends Build {

  override val settings: Seq[sbt.Def.Setting[_]] = (super.settings ++ Ajustes.deConstruccion).toSeq

  private val v = "0.0.1"

  // -------------------------------------------------------------------------------------------------------------------
  // Root Project
  // -------------------------------------------------------------------------------------------------------------------
  lazy val root = Project(id = "Solveet", base = file("."), settings = Ajustes.basicos.toSeq)
    .settings(libraryDependencies ++= Dependencias.base)
    .aggregate(problemas)

  // -------------------------------------------------------------------------------------------------------------------
  // Modules
  // -------------------------------------------------------------------------------------------------------------------
  lazy val problemas = Modulo("problemas", v)

  private def Modulo(nombre: String, versionModulo: String, extraLibs:  Traversable[ModuleID] = Nil): Project = {
    Project(nombre, base=file(nombre), settings=ajustesBasicos(versionModulo, Nil, extraLibs))
  } //def

  private def ajustesBasicos(versionModulo: String,
                             extraAjustes: Seq[sbt.Def.Setting[_]],
                             extraLibs:  Traversable[ModuleID]): Seq[sbt.Def.Setting[_]] = {
    Ajustes.basicos ++ extraAjustes ++
      Seq(version := versionModulo, libraryDependencies ++= Dependencias.base ++ extraLibs)
  } //def
}	//object
