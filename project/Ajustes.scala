import sbt._
import Keys._

/**
* Author: Piousp
* Company: Ciris Informatic Solutions
*/
object Ajustes {

  private val organizacion              = "com.ciriscr.solveet"
  private val versionScala              = "2.11.6"
  private val opcionesScalacBase        = Seq(
    "-encoding", "utf8",
    "-feature",
    "-unchecked",
    "-deprecation"
  )
  private val opcionesScalacExtra 			= Seq(
      "-target:jvm-1.7",
      "-language:postfixOps",
      "-language:implicitConversions",
      "-Xlog-reflective-calls"
  )
  private val opcionesJavac				      = Seq(
    "-source", "1.7",
    "-target", "1.7",
    "-Xlint:unchecked",
    "-Xlint:deprecation",
    "-Xlint:-options"
  )


  val basicos = Seq(
    organization      := organizacion,
    scalaVersion      := versionScala,
    startYear         := Some(2014),
    scalacOptions     := opcionesScalacBase ++ opcionesScalacExtra,
    javacOptions      := opcionesJavac,
    resolvers         := Resolvers.basic,
    retrieveManaged   := true
  ) ++ org.scalastyle.sbt.ScalastylePlugin.Settings

  val deConstruccion = Seq(
    shellPrompt               := ShellPrompt.buildShellPrompt,
    scalaVersion in ThisBuild := versionScala,
    scalacOptions             := opcionesScalacBase,
    javacOptions              := opcionesJavac
  )

  object ShellPrompt {
    object devnull extends ProcessLogger {
      def info (s: => String) {}
      def error (s: => String) { }
      def buffer[T] (f: => T): T = f
    }
    def currBranch = (
      ("git status -sb" lines_! devnull headOption)
        getOrElse "-" stripPrefix "## "
      )

    val buildShellPrompt = {
      if(System.getProperty("os.name").toLowerCase.contains("windows"))
        (state: State) => "%s >".format(Project.extract (state).currentProject.id)
      else {
        (state: State) => {
          val currProject = Project.extract (state).currentProject.id
          "%s:%s> ".format (currProject, currBranch)
        }
      } //def
    }

  }  //object
}	//object
