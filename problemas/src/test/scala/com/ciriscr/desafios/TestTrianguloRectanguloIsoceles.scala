package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 03/09/12
 * Time: 01:14 PM
 */

class TestTrianguloRectanguloIsoceles extends FunSuite with TrianguloRectanguloIsoceles {

  test("crear triangulo"){
    println("cateto = 4")
    assert(res1 === crearTriangulo(1))
    assert(res2 === crearTriangulo(2))
    assert(res3 === crearTriangulo(3))
    assert(res4 === crearTriangulo(4))
  }


  val res1 = """X
"""
  val res2 = """X
XX
"""
  val res3 = """X
XX
XXX
"""
  val res4 = """X
XX
XXX
XXXX
"""

}
