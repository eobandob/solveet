package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        5/7/14
 * Hora:         11:40 AM
 */

class TestJuggler extends FunSuite {
  test("ejemplo") {
    val j = Juggler(3)
    assert(j.secuencia === "3, 5, 11, 36, 6, 2, 1")
    assert(j.longitud === 6)
    assert(j.mayor === 36)
  }
}