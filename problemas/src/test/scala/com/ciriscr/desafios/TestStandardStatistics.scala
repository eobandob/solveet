package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programmer:  londo
 * Date:        14/05/15
 * Time:        03:05 PM
 */
class TestStandardStatistics extends FunSuite {
  abstract class Prueba(lista: List[Long]) {
    def rMax: Long
    def rSize: Int
    def rMin: Long
    def rMedia: Double
    def rMediana: Double
    def rModa: Set[Long]
    def rDesviacionEstandar: Double
    def rDesviacionMedia: Double
    def rVarianza: Double
    def rRango: Long

    private lazy val ss = StandardStatistics(lista)

    def validarRespuesta(r1: Double, r2: Double): Boolean = r1 - r2 < 0.00001

    assert(ss.moda === rModa, s"Moda. ${ss.moda} es diferente de $rModa")
    assert(validarRespuesta(ss.desviacionEstandar, rDesviacionEstandar), s"Desviacion Estandar. ${ss.desviacionEstandar} es diferente de $rDesviacionEstandar")
    assert(validarRespuesta(ss.desviacionMedia, rDesviacionMedia), s"Desviacion Media. ${ss.desviacionMedia} es diferente de $rDesviacionMedia")
    assert(validarRespuesta(ss.varianza, rVarianza), s"Varianza. ${ss.varianza} es diferente de $rVarianza")
    assert(ss.rango === rRango, s"Rango. ${ss.rango} es diferente de $rRango")
    assert(ss.max === rMax, s"Max. ${ss.max} es diferente de $rMax")
    assert(ss.min === rMin, s"Min. ${ss.max} es diferente de $rMin")
    assert(ss.size === rSize, s"Size. ${ss.max} es diferente de $rSize")
    assert(validarRespuesta(ss.media, rMedia), s"Media. ${ss.media} es diferente de $rMedia")
    assert(validarRespuesta(ss.mediana, rMediana), s"Mediana. ${ss.mediana} es diferente de $rMediana")
  }

  test("Lista 1") {
    new Prueba(List(3, 3, 26, 17, 3, 7)) {
      def rRango: Long = 23
      def rMedia: Double = 9.8333333
      def rMax: Long = 26
      def rDesviacionEstandar: Double = 9.6003472
      def rModa: Set[Long] = Set(3)
      def rVarianza: Double = 92.1666666667
      def rDesviacionMedia: Double = 7.7777777777778
      def rMin: Long = 3
      def rSize: Int = 6
      def rMediana: Double = 5
    }
  }

  test("Lista 2") {
    new Prueba(List(18, 28, 14, 16, 26, 25, 3, 5, 25)) {
      def rRango: Long = 25
      def rMedia: Double = 17.7777778
      def rMax: Long = 28
      def rDesviacionEstandar: Double = 9.1893658
      def rModa: Set[Long] = Set(25)
      def rVarianza: Double = 84.4444444444
      def rDesviacionMedia: Double = 8.6638171954
      def rMin: Long = 3
      def rSize: Int = 9
      def rMediana: Double = 18
    }
  }

  test("Lista 3") {
    new Prueba(List(26, 7, 5, 23, 2)) {
      def rRango: Long = 24
      def rMedia: Double = 12.6000000
      def rMax: Long = 26
      def rDesviacionEstandar: Double = 11.0589330
      def rModa: Set[Long] = Set.empty[Long]
      def rVarianza: Double = 122.3
      def rDesviacionMedia: Double = 9.8914104151
      def rMin: Long = 2
      def rSize: Int = 5
      def rMediana: Double = 7
    }
  }

  test("Lista 4") {
    new Prueba(List(25, 24, 29)) {
      def rRango: Long = 5
      def rMedia: Double = 26.0000000
      def rMax: Long = 29
      def rDesviacionEstandar: Double = 2.6457513
      def rModa: Set[Long] = Set.empty[Long]
      def rVarianza: Double = 7
      def rDesviacionMedia: Double = 2.1602468995
      def rMin: Long = 24
      def rSize: Int = 3
      def rMediana: Double = 25
    }
  }

  test("Lista 5") {
    new Prueba(List(23, 6, 18, 25, 3, 19, 29, 11, 23, 2, 6, 10, 5, 14, 28, 2, 28, 14, 29)) {
      def rRango: Long = 27
      def rMedia: Double = 15.5263158
      def rMax: Long = 29
      def rDesviacionEstandar: Double = 9.9015623
      def rModa: Set[Long] = Set(2, 28, 14, 29, 6, 23)
      def rVarianza: Double = 98.0409356725
      def rDesviacionMedia: Double = 9.6374730312
      def rMin: Long = 2
      def rSize: Int = 19
      def rMediana: Double = 14
    }
  }
}