package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 09:40 PM
 */

class TestCalculadoraNumerosEnormes extends FunSuite with CalculadoraNumerosEnormes {

  test("Sumas enormes") {
    assert(realizarOperacion("5 + 5") === "5 + 5 = 10")
    assert(realizarOperacion("134897248723489123458761234323 + 89236667128712672387234656256813487") === "5 + 5 = 10")
  }

}
