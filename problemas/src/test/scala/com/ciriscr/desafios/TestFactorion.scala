package com.ciriscr.desafios

import org.scalatest.FunSuite
import java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 31/05/12
 * Time: 08:25 PM
 */

class TestFactorion extends FunSuite with RunFactorion {

  test("factorion") {
    val ini = new Date().getTime
    procesar
    val fin = new Date().getTime
    println(fin - ini)
  }
}
