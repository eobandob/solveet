package com.ciriscr.desafios

import org.scalatest.FunSuite

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 31/05/12
 * Time: 08:25 PM
 */

class TestArbolNavidad extends FunSuite with ArbolNavidad {

  test("ArbolNavidad"){
    assert(generateTree(1) === "*")
    assert(generateTree(2) === " *\n***")
    assert(generateTree(3) === "  *\n ***\n*****")
    assert(generateTree(4) === "   *\n  ***\n *****\n*******")
  }
}
