package com.ciriscr.desafios

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programmer:  londo
 * Date:        14/05/15
 * Time:        02:17 PM
 */
case class StandardStatistics(l: List[Long]) {
  def +(num: Long): StandardStatistics = StandardStatistics(num :: l)
  def +(num: Int): StandardStatistics = this + num.toLong
  def eliminarAnterior: StandardStatistics = StandardStatistics(l.tail)
  def eliminarPrimero: StandardStatistics = StandardStatistics(l.init)

  def size: Int = l.size
  def max: Long = l.max
  def min: Long = l.min
  def media: Double = 1.0 * l.sum / l.size
  def mediana: Double = {
    val ls = l.sorted
    if (ls.size % 2 == 1)
      ls(ls.size / 2)
    else {
      val mitad = math.floor(ls.size / 2.0).toInt - 1
      val v1 = ls(mitad)
      val v2 = ls(mitad + 1)
      (v1 + v2) / 2.0
    }
  }
  def moda: Set[Long] = {
    val mapeo = l.groupBy(l => l)
    val l2 = mapeo.map(t => t._1 -> t._2.size).toList.groupBy(_._2)
    val keys = l2.keySet
    if (keys.size <= 1)
      Set.empty[Long]
    else
      l2(keys.max).map(_._1).toSet
  }
  def desviacionEstandar: Double = math.sqrt(varianza)
  def desviacionMedia: Double = {
    val prom = media
    l.map(n => n - prom).sum / l.size
  }
  def varianza: Double = {
    val prom = media
    l.map(n => (n - prom) * (n - prom)).sum / l.size
  }
  def rango: Long = l.max - l.min

  override def toString: String = {
    s"""Tamaño: $size
      |Max: $max
      |Min: $min
      |Media: $media
      |Mediana: $mediana
      |Moda: $moda
      |Desviación estándar: $desviacionEstandar
      |Desviación media: $desviacionMedia
      |Varianza: $varianza
      |Rando: $rango
    """.stripMargin
  }
}