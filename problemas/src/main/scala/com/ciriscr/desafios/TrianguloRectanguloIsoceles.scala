package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 03/09/12
 * Time: 12:37 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait TrianguloRectanguloIsoceles {

  def crearTriangulo(cateto: Int) = crear(cateto, 1)

  private def crear(cateto: Int, n: Int):String =
    if (n-1 == cateto) "" else ("X" * n) + "\n" + crear(cateto, n+1)

}
