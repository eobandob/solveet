package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 14/06/12
 * Time: 09:48 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait CalculadoraNumerosEnormes {

  implicit def StringToBigDecimal(s: String): BigDecimal = BigDecimal(s)

  def realizarOperacion(formula: String) = {
    val s = formula.split(' ')
    if (s.isDefinedAt(2)){
      val op = operacion(s(0), s(2))
      if (op.isDefinedAt(s(1)))
        formula + " = " + op(s(1))
      else
        "Operación no soportada"
    } else
      "Error al escribir la fórmula"
  }

  private def operacion(num1: BigDecimal, num2: BigDecimal): PartialFunction[String, BigDecimal] = {
    case "+" => num1 + num2
    case "-" => num1 - num2
    case "*" => num1 * num2
    case "/" => num1 / num2
    case "min" => num1 min num2
    case "max" => num1 max num2
    case "%" => num1 % num2
    case "mod" => num1 % num2
  }
}
