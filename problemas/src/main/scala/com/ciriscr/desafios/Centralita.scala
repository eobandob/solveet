package com.ciriscr.desafios

import io.Source

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 14/06/12
 * Time: 08:17 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait Central {

  private val tarifas = Map(TipoLlamada.Local -> 0.15, TipoLlamada.Prov1 -> 0.20, TipoLlamada.Prov2 -> 0.25,
                            TipoLlamada.Prov3 -> 0.30)

  def facturar(file: String) = {
    val llamadas = new ReaderCentralita(file).read
    llamadas.map(_.numeroOrigen).distinct.map{
      num => num +": "+ llamadas.filter(_.numeroOrigen == num).foldLeft(0.0)((a, b) => a + b.duracion * tarifas(b.tipoLlamada))
    }.mkString("\n")
  }

}

private class ReaderCentralita(filePath: String) {
  def read = new Reader(filePath).read.toList.map{
    l =>
      val s = l.split(';')
      new Llamada(s(0), s(1), s(2).toInt, TipoLlamada.withName(s(3)))
  }
}

class Llamada(val numeroOrigen: String, val numeroDestino: String, val duracion: Int,
              val tipoLlamada: TipoLlamada.Value)

object TipoLlamada extends Enumeration {
  val Local = Value("Local")
  val Prov1 = Value("Provincia, franja 1")
  val Prov2 = Value("Provincia, franja 2")
  val Prov3 = Value("Provincia, franja 3")
}

//Archivo fuente
/*
8418;1248;32;Provincia, franja 1
8418;6342;609;Local
1248;1297;334;Local
6836;9363;414;Provincia, franja 2
1248;8418;72;Provincia, franja 3
5342;2857;932;Local
1248;8418;163;Provincia, franja 2
*/