package com.ciriscr.desafios

/**
 * Ciris Informatic Solutions
 * --------------------------
 * www.ciriscr.com
 * Programador:  londo 
 * Fecha:        5/7/14
 * Hora:         10:49 AM
 */

case class Juggler(n: Int) {
  private lazy val serie: List[Int] = n :: {
    def stream(m: Int): Stream[Int] = {
      if (m == 1) Stream.empty[Int]
      else {
        val a = if (m % 2 == 0) {
          math.pow(m, 0.5).toInt
        } else {
          math.pow(m, 1.5).toInt
        }
        a #:: stream(a)
      }
    }
    stream(n).toList
  }

  def secuencia: String = serie.mkString(", ")
  def longitud: Int = serie.length - 1
  def mayor: Int = serie.max

}
