package com.ciriscr.desafios

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 01/06/12
 * Time: 09:38 PM
 */

//código fuente; https://bitbucket.org/londo/solveet-desafios
trait PotenciasDos {

  def potencia(n: Int) = {
    require(n >= 0 && n <= 256)
    BigInt(2).pow(n)
  }

}
